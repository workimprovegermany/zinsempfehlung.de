<?php
/**
 * Created by PhpStorm.
 * User: marco
 * Date: 11.10.17
 * Time: 17:14
 */

class Mailchimp
{
    private $mc_base_url;
    private $mc_api_key;
    private $mc_timeout;

    private $mc_list_id;

    public function __construct($config)
    {
        //TODO: test if $config contains valid values
        $this->mc_base_url = $config[''];
        $this->mc_api_key = $config['mc_api_key'];
        $this->mc_timeout = $config['mc_timeout'] / 1000;

        //$this->mc_list_id = $this->get_first_list_id();
        $this->mc_list_id = $config['mc_list_id'];
    }

    public function set_list_id($id)
    {
        $this->mc_list_id = $id;
    }

    public function create_update_user($teilnehmer, $export_config)
    {
        $export_fields = array(
            "vorname" => "Vorname",
            "name" => "Name",
            "email" => "E-Mail",
            "anrede" => "Anrede",
            "ansprache" => "Ansprache",
            "videotitel" => "Videotitel",
            "videountertitel" => "Videountertitel",
            "videoid" => "Webinar ID",
            "videodatum" => "Datum Teilnahme",
            "teilgenommen" => "teilgenommen",
            "strasse" => "Straße",
            "plz" => "PLZ",
            "ort" => "Ort",
        );

        // sollen in db stehen
        $imparare_field_names = array(
            "vorname" => "vorname",
            "name" => "name",
            "email" => "email",
            "anrede" => "anrede",
            "ansprache" => "ansprache",
            "videotitel" => "video_title",
            "videountertitel" => "video_untertitel",
            "videoid" => "video_id",
            "videodatum" => "teilgenommen_datum",
            "teilgenommen" => "teilgenommen",
            "strasse" => "strasse",
            "plz" => "plz",
            "ort" => "ort",
        );

        $required_export_fields = array('vorname', 'name', 'email');

        $export = array();
        foreach ($export_fields as $field => $descr)
        {
            // wenn Pflichtfeld oder Exporthaken gesetzt ist
            if (in_array($field, $required_export_fields) || $export_config[api_table_export_column_name($field)] == 1)
            {
                // übernehmen bedeutet im array zu setzen: mailer_feldname => wert_von(imparare_feldname)
                $export[$export_config[api_table_mailer_column_name($field)]] = $teilnehmer->nice_field($imparare_field_names[$field]);
            }
        }


        /*$data = array(
            "email" => $teilnehmer->field('email'),
            "merge_fields" => array(
                "NAME"=>$teilnehmer->field('name'),
                "VORNAME"=>$teilnehmer->field('vorname'),
                "TELEFON"=>$teilnehmer->field('telefon'),
                "ANREDE"=>$teilnehmer->field('anrede') == 1 ? 'Herr' : 'Frau',
                "ANSPRACHE"=>$teilnehmer->field('ansprache'),
                "IP"=>$teilnehmer->field('teilgenommen_ip'),
                "SESSION"=>$teilnehmer->field('teilgenommen_session'),
                "ZUORDNUNG"=>strtoupper($teilnehmer->field('zuordnung')),
                "FIRMA"=>$teilnehmer->field('firma'),
                "VONSEITE"=>$teilnehmer->field('video_id'),
                //"SMS"=>' ',
            ),
        );*/
        $data = array(
            "email_address" => $teilnehmer->field('email'),
            "status" => "subscribed",
            "merge_fields" => $export,
        );

        print_r($data);

        $ch = curl_init($this->mc_base_url."lists/".$this->mc_list_id."/members/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $auth = 'user:'.$this->mc_api_key;
        curl_setopt($ch, CURLOPT_USERPWD, $auth);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        //for debug
        //curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);


        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch);
        curl_close($ch);

        $res_dec = json_decode($result);

        echo "<pre>";
        print_r($res_dec);
        echo "</pre>";

        echo "<pre>";
        print_r($httpCode);
        echo "</pre>";

        return true;
    }

    public function test()
    {

        $ch = curl_init($this->mc_base_url."lists/".$this->mc_list_id."/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $auth = 'user:'.$this->mc_api_key;
        curl_setopt($ch, CURLOPT_USERPWD, $auth);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        //for debug
        //curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);

        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch);
        curl_close($ch);

        $res_dec = json_decode($result);

        echo "<pre>";
        print_r($res_dec);
        echo "</pre>";

        echo "<pre>";
        print_r($httpCode);
        echo "</pre>";
    }

    function get_first_list_id()
    {
        $ch = curl_init('https://us16.api.mailchimp.com/3.0/lists');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $auth = 'user:'.$this->mc_api_key;
        curl_setopt($ch, CURLOPT_USERPWD, $auth);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        //for debug
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);

        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch);
        curl_close($ch);

        $res_dec = json_decode($result);
        return $res_dec->lists[0]->id;
    }
}

function api_table_export_column_name($fieldname)
{
    return "p_".$fieldname."_export";
}
function api_table_imparare_column_name($fieldname)
{
    return "p_".$fieldname."_imparare";
}
function api_table_mailer_column_name($fieldname)
{
    return "p_".$fieldname."_mailer";
}

