<?php

class Get_response
{
    private $base_url;
    private $api_id;

    private $list_id;

    public function __construct($config)
    {
        //TODO: test if $config contains valid values
        $this->base_url = 'https://api.getresponse.com/v3';
        $this->api_id = $config['api_id'];

        $this->list_id = $config['list_id'];
    }

    public function set_list_id($id)
    {
        $this->list_id = $id;
    }

    public function create_update_user($teilnehmer, $export_config)
    {
        $export_fields = array(
            "vorname" => "Vorname",
            "name" => "Name",
            "email" => "E-Mail",
            "anrede" => "Anrede",
            "ansprache" => "Ansprache",
            "videotitel" => "Videotitel",
            "videountertitel" => "Videountertitel",
            "videoid" => "Webinar ID",
            "videodatum" => "Datum Teilnahme",
            "teilgenommen" => "teilgenommen",
            "strasse" => "Straße",
            "plz" => "PLZ",
            "ort" => "Ort",
        );

        // sollen in db stehen
        $imparare_field_names = array(
            "vorname" => "vorname",
            "name" => "name",
            "email" => "email",
            "anrede" => "anrede",
            "ansprache" => "ansprache",
            "videotitel" => "video_title",
            "videountertitel" => "video_untertitel",
            "videoid" => "video_id",
            "videodatum" => "teilgenommen_datum",
            "teilgenommen" => "teilgenommen",
            "strasse" => "strasse",
            "plz" => "plz",
            "ort" => "ort",
        );

        $required_export_fields = array('vorname', 'name', 'email');

        $name_field = '';
        // put vorname+name in field name if selected
        // name is a predefined optional field in Get_response
        if ($export_config[api_table_mailer_column_name('vorname')] == 'name' || $export_config[api_table_mailer_column_name('name')] == 'name')
        {
            if ($export_config[api_table_mailer_column_name('vorname')] == 'name') $name_field .= $teilnehmer->nice_field($imparare_field_names['vorname']);
            if ($export_config[api_table_mailer_column_name('name')] == 'name' && $export_config[api_table_mailer_column_name('name')] == 'name') $name_field .= ' ';
            if ($export_config[api_table_mailer_column_name('name')] == 'name') $name_field .= $teilnehmer->nice_field($imparare_field_names['name']);
        }

        $export = array();
        foreach ($export_fields as $field => $descr)
        {
            // wenn Pflichtfeld oder Exporthaken gesetzt ist
            if (in_array($field, $required_export_fields) || $export_config[api_table_export_column_name($field)] == 1)
            {
                if ($field == 'vorname' && $export_config[api_table_mailer_column_name('vorname')] == 'name' || $field == 'name' && $export_config[api_table_mailer_column_name('name')] == 'name' || $field == 'email')
                {

                }
                else
                {
                    // übernehmen bedeutet im array zu setzen: mailer_feldname => wert_von(imparare_feldname)
                    //$export[$export_config[api_table_mailer_column_name($field)]] = $teilnehmer->nice_field($imparare_field_names[$field]);
                    $new_entry = new stdClass();
                    $new_entry->customFieldId = $export_config[api_table_mailer_column_name($field)];
                    $new_entry->value = array();
                    $val = $teilnehmer->nice_field($imparare_field_names[$field]);
                    if (!empty($val))
                    {
                        array_push($new_entry->value, $teilnehmer->nice_field($imparare_field_names[$field]));
                        array_push($export, $new_entry);
                    }
                }
            }
        }


        /*$data = array(
            "email" => $teilnehmer->field('email'),
            "merge_fields" => array(
                "NAME"=>$teilnehmer->field('name'),
                "VORNAME"=>$teilnehmer->field('vorname'),
                "TELEFON"=>$teilnehmer->field('telefon'),
                "ANREDE"=>$teilnehmer->field('anrede') == 1 ? 'Herr' : 'Frau',
                "ANSPRACHE"=>$teilnehmer->field('ansprache'),
                "IP"=>$teilnehmer->field('teilgenommen_ip'),
                "SESSION"=>$teilnehmer->field('teilgenommen_session'),
                "ZUORDNUNG"=>strtoupper($teilnehmer->field('zuordnung')),
                "FIRMA"=>$teilnehmer->field('firma'),
                "VONSEITE"=>$teilnehmer->field('video_id'),
                //"SMS"=>' ',
            ),
        );*/

        $data = new stdClass();
        $data->email = $teilnehmer->field('email');
        if (!empty($name_field)) $data->name = $name_field;
        $data->campaign = new stdClass();
        $data->campaign->campaignId = $this->list_id;
        $data->customFieldValues = $export;
        $data->dayOfCycle = '0';

        echo "export data json:";
        echo "<pre>";
        print_r(json_encode($data));
        echo "</pre>";

        try
        {
            $rc = new Rest($this->base_url);
            $rc->set_auth_mode('x_auth_token', $this->api_id);
            $res_dec = $rc->post('/contacts', $data);

            echo "<pre>";
            echo "TEST GETRESPONSE";
            print_r($res_dec);
            echo "</pre>";
        }
        catch (Exception $e)
        {
            echo "ERROR";
            echo "<pre>";
            print_r($e->getMessage());
            echo "</pre>";
        }

        return true;
    }

    function get_first_list_id()
    {
        /*$ch = curl_init('https://us16.api.mailchimp.com/3.0/lists');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $auth = 'user:'.$this->mc_api_key;
        curl_setopt($ch, CURLOPT_USERPWD, $auth);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        //for debug
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);

        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch);
        curl_close($ch);

        $res_dec = json_decode($result);
        return $res_dec->lists[0]->id;*/
    }

    function get_lists()
    {
        try
        {
            $rc = new Rest($this->base_url);
            $rc->set_auth_mode('x_auth_token', $this->api_id);
            $res_dec = $rc->get('/campaigns');

            //echo "GRUPPEN<br/>";
            //echo "<pre>";
            //print_r($res_dec);
            //echo "</pre>";

            $ret = array();
            foreach ($res_dec as $item)
            {
                $entry = new stdClass();
                $entry->id = $item->campaignId;
                $entry->name = $item->name;
                array_push($ret, $entry);
            }

            return $ret;
        }
        catch (Exception $e)
        {
            echo "ERROR";
            echo "<pre>";
            print_r($e->getMessage());
            echo "</pre>";
        }
    }

    function get_fields()
    {
        try
        {
            $rc = new Rest($this->base_url);
            $rc->set_auth_mode('x_auth_token', $this->api_id);
            $res_dec = $rc->get('/custom-fields');

            $ret = array();

            $entry = array();
            $entry['name'] = 'name';
            $entry['description'] = 'Name (Vor- und Nachname)';

            array_push($ret, $entry);
            foreach ($res_dec as $item)
            {
                $entry = array();
                $entry['name'] = $item->customFieldId;
                $entry['description'] = $item->name;
                array_push($ret, $entry);
            }

            return $ret;
        }
        catch (Exception $e)
        {
            echo "ERROR";
            echo "<pre>";
            print_r($e->getMessage());
            echo "</pre>";
        }
    }

    function get_contacts()
    {
        try
        {
            $rc = new Rest($this->base_url);
            $rc->set_auth_mode('x_auth_token', $this->api_id);
            $res_dec = $rc->get('/contacts');

            return $res_dec;
        }
        catch (Exception $e)
        {
            echo "ERROR";
            echo "<pre>";
            print_r($e->getMessage());
            echo "</pre>";
        }
    }
}


function api_table_export_column_name($fieldname)
{
    return "p_".$fieldname."_export";
}
function api_table_imparare_column_name($fieldname)
{
    return "p_".$fieldname."_imparare";
}
function api_table_mailer_column_name($fieldname)
{
    return "p_".$fieldname."_mailer";
}
