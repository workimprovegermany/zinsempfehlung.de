<?php
/**
 * Created by PhpStorm.
 * User: marco
 */

class Entity_view
{
    private $app;

    public function __construct($app)
    {
        $this->app = $app;
    }

    public function render_field($field, $e)
    {

        $name = $field->name[0];
        $desc = $field->description[0];
        $type = $field->type[0];
        $value = $e->get_field($name->__toString());

        echo "<div class=\"form-group\">";
        echo "<label for=\"$name\">$desc</label>";

        if ($type == 'option')
        {
            echo "<select class=\"form-control\" name=\"$name\" id=\"$name\">";

            foreach ($field->options->option as $option)
            {
                echo "<option>$option[0]</option>";
            }

            echo "</select>";
        }
        elseif ($type == 'number')
        {
            $unit = $field->unit[0];

            if ($unit == 'years')
            {
                echo "<div class=\"input-group\">";
                echo "<input type=\"number\" name=\"$name\" id=\"$name\" class=\"form-control\" aria-label=\"Jahre\" value=\"$value\">";
                echo "<span class=\"input-group-addon\">Jahre</span>";
                echo "</div>";
            }
            elseif ($unit == 'euro')
            {
                echo "<div class=\"input-group\">";
                echo "<input type=\"number\" name=\"$name\" id=\"$name\" class=\"form-control\" aria-label=\"Euro\" value=\"$value\">";
                echo "<span class=\"input-group-addon\">€</span>";
                echo "</div>";
            }
            elseif ($unit == 'percent')
            {
                echo "<div class=\"input-group\">";
                echo "<input type=\"number\" name=\"$name\" id=\"$name\" class=\"form-control\" aria-label=\"Prozent\" value=\"$value\">";
                echo "<span class=\"input-group-addon\">%</span>";
                echo "</div>";
            }
            else
            {
                echo "<div>unbekannte Einheit: $unit</div>";
            }
        }
        else
        {
            echo "<div>unbekannter Typ: $type</div>";
        }

        echo "</div>";
    }

    public function render_entities_connected_to($foreign_cfg, $foreign_id, $entity_name)
    {
        // Renders all $entity_name, that are connected to $foreign_cfg with ID $foreign_cfg

        echo "<button onclick=\"new_clicked('$entity_name')\">neue $entity_name anlegen</button>";

        $fec = new Entity_config($foreign_cfg);
        $mapper = new Entity_mapper($this->app->db());
        $mapper->set_config($fec);
        $f_obj = $mapper->find_by_id($foreign_id);

        $r_objs = $f_obj->get_related($entity_name);
        foreach ($r_objs as $r_obj)
        {
            $this->render_entity($entity_name, $r_obj->get_field('id'));
        }
    }

    public function render_entity($entity_name, $id)
    {
        //echo "render $entity_name : $id";

        $config_xml = new SimpleXMLElement(file_get_contents("../models/".$entity_name."/1.0/config.xml"));
        $fields = $config_xml->xpath('/config/fields/field[description]');

        $em = new Entity_mapper($this->app->db(),$entity_name."/1.0/config.xml");
        $e = $em->find_by_id($id, $entity_name);

        echo "<div class=\"entity\">";

        $title = !empty($e->get_field('einheitenbezeichnung')) ? $e->get_field('einheitenbezeichnung') : $entity_name;

        echo "<div class=\"entity-title\">";
        echo "<p>".$title."<span class=\"glyphicon glyphicon-trash entity-action-icon\" aria-hidden=\"true\" onclick=\"delete_clicked('$entity_name', $id)\"></span></p>";
        echo "</div>";


        echo "<form action=\"http://immo.hosting104201.af9ae.netcup.net/rechner/views/api.php\" method=\"post\">";
        foreach ($fields as $field)
        {
            $this->render_field($field, $e);
        }
        echo "</form>";

        echo "<button onclick=\"save_clicked()\">speichern</button>";

        echo "</div>";
    }

    public function show($html = '')
    {
        $entity_name = CONTROLLER_NAME;
        $id = CONTROLLER_ACTION;
        $params = CONTROLLER_PARAMETER;

        define('NESTED_PAGE', CONTROLLER_ACTION !== 'show');

        $f_cfg = $_POST['foreign_entity']."/1.0/config.xml";

        ?>
        <!DOCTYPE html>
        <html lang="de">

        <?php
        include("../views/partials/head.php");
        ?>

        <body>

        <div class="entity-container">

            <?php
            if ($id == 'show')
            {
                //multiple entities
                $this->render_entities_connected_to($f_cfg,$_POST['foreign_id'],$entity_name);
            }
            else
            {
                //single entity
                //echo "single <br/>";
                $this->render_entity($entity_name, $id);
            }
            ?>

        </div>

        <div class="entity-container">

        <?php
        $e_cfg = $entity_name."/1.0/config.xml";
        $config_xml = new SimpleXMLElement(file_get_contents("../models/".$e_cfg));
        $associated_entities = $config_xml->xpath('/config/associated_entities/entity');

        foreach ($associated_entities as $ae)
        {
            if (!empty($ae['related_to']))
            {
                $a_cfg = $ae['related_to'] . "/1.0/config.xml";
                $this->render_entities_connected_to($a_cfg, $_POST['foreign_id'], $ae);
            }
            else
            {
                $this->render_entities_connected_to($e_cfg, $id, $ae);
            }
        }

        ?>

        </div>

        <script src="<?php if (NESTED_PAGE==true) echo '../'?>public/js/bootstrap-slider.js"></script>

        <script>
            save_clicked = function () {
                form = $("form").serializeArray();
                data = {"action":"save", "entity":"objekt"};
                $(form).each(function(index, obj)
                {
                    data[obj.name] = obj.value;
                });
                console.log(data);
                $.post("http://immo.hosting104201.af9ae.netcup.net/rechner/views/api.php",
                    data,
                    function(data) {
                        console.log(data);
                        var url = "http://immo.hosting104201.af9ae.netcup.net/rechner/berechnung/1";
                        window.location.href = url;
                    },
                    "json"
                );
            };

            new_clicked = function (entity) {
                var fe = entity == 'erwerber' ? 'berechnung_id' : 'objekt_id';
                var fields = {};
                fields[fe] = "1";
                var data = {"action":"new", "entity":entity, "fields": fields};
                $.post("http://immo.hosting104201.af9ae.netcup.net/rechner/views/api.php",
                    data,
                    function(data) {
                        console.log(data);
                        window.location.reload();
                    },
                    "json"
                );
            };

            delete_clicked = function (entity_name, id) {
                data = {"action":"delete", "entity":entity_name, "id":id};
                $.post("http://immo.hosting104201.af9ae.netcup.net/rechner/views/api.php",
                    data,
                    function(data) {
                        console.log(data);
                        window.location.reload();
                    },
                    "json"
                );
            }

            //$(document).ready(function()
            //{
            //    $(".dropdown-toggle").dropdown();
            //}
        </script>


        <!-- Bootstrap core JavaScript
================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <script src="../vendor/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <!--script src="bootstrap-3.3.7-dist/assets/js/ie10-viewport-bug-workaround.js"></script-->

        </body>

        </html>

        <?php
    }

    private function debug($txt, $obj)
    {
        echo "<pre>";
        echo $txt."<br/>";
        print_r($obj);
        echo "</pre>";
    }
}