<?php

class Route
{
    private $url = '';
    private $controller = '';
    private $action = '';

    private $parameter = array();
    private $url_pattern = array();

    function __construct($url, $controller = 'default', $action = 'show')
    {
        $this->url = $url;
        $this->controller = $controller;
        $this->action = $action;
    }


    /*************** GETTERS & SETTERS *******************/

    /**
     * @return string
     */
    public function url()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function set_url($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function controller()
    {
        return $this->controller;
    }

    /**
     * @param string $controller
     */
    public function set_controller($controller)
    {
        $this->controller = $controller;
    }

    /**
     * @return array
     */
    public function parameter()
    {
        return $this->parameter;
    }

    /**
     * @return array
     */
    public function url_pattern()
    {
        return $this->url_pattern;
    }


    public function set_matches($matches)
    {
        foreach ($matches as $key => $value)
        {
            if (is_numeric($key))
            {
                $this->url_pattern[$key] = $value;
            }
            else
            {
                $this->parameter[$key] = $value;
            }
        }

        // TODO UGLY!!!
        $filter = "";
        $ps = $this->parameter;
        foreach ($ps as $k => $v)
        {
            $filter .= $k . ':' . $v;
        }
        define('GLOBAL_FILTER_STRING', $filter);

        define('CONTROLLER_ACTION', $matches[2]);
    }



}