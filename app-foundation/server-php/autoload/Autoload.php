<?php

class Autoload{
    private $autoloadable = [];

    public function register($name, $loader = false){
        if( is_callable($loader) || $loader == false){
            $this->autoloadable[$name] = $loader;
            return;
        }
        throw new Exception('Loader must be callable '.$name);
    }

    public function load($name_with_namespace)
    {
        $parts = explode('\\', $name_with_namespace);
        $name = end($parts);

        if( !empty($this->autoloadable[$name]) )
        {
            return $this->autoloadable[$name]($name);
        }

        $filepath = APP_FOUNDATION_SERVER_PATH.'/'.strtolower($name).'/'.$name.'.php';
        if( file_exists($filepath) )
        {
            return require($filepath);
        }

        if (strpos($name,'controller') !== false)
        {
            $filepath = APP_FOUNDATION_SERVER_PATH.'/controller/'.$name.'.php';
        }
        else
        {
            //fallback to other php path
            $filepath = APP_FOUNDATION_SERVER_PATH.'/../classes'.$name.'.php';
        }

        if( file_exists($filepath) ){
            return require($filepath);
        }
        throw new Exception($name.' is not loaded or registred for autoloading'.' filepath: '.$filepath);
    }
}