<?php


class Login_failed_action extends AF\Action implements Event_notification_interface
{
    public static function get_events_to_notify()
    {
        return array('user.login_failed');
    }

    public function execute()
    {
        $user_name = $this->data;

        $em = new Entity_mapper($this->app->db(), 'user_login/1.0/config.xml');
        $fields = array(
            'uname' => $user_name,
        );

        $user_login_array = $em->find_by_fields($fields);

        if ($user_login_array !== null) {
            //user found in db
            $user_login = $user_login_array[0];

            //if password not correct
            $pass_falsch = $user_login->field('falsch');
            //$user_login->set_field('falsch', $pass_falsch + 1);
            $em->save($user_login);

            // if password is 4 and incorrect
            if ($pass_falsch == 4)
            {
                $user_login->set_field('aktiv', 0);
                $em->save($user_login);

                $mail_event = new Event('mail.send', [
                    'config' => 'supportmails',
                    'template' => ['mails/user_blocked.html',
                        [
                            'user_login' => $user_login->id(),
                        ]
                    ],
                ]);
                $this->app->dispatch_event($mail_event);
            }
        }
    }
}
