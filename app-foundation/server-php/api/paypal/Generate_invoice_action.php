<?php

class Generate_invoice_action extends AF\Action
{
    public function execute()
    {
        //test: http://192.168.240.113/imparare.de/api?&af_cmd=paypal_generate_invoice&af_data=%7B%22invoice_number%22%3A%2220180612143013%22%7D
        //test: http://192.168.240.113/imparare.de/api?&af_cmd=paypal_generate_invoice&af_data=%7B%22order_id%22%3A%222%22%7D
        $order_id = $this->data['order_id'];

        $em = new Entity_mapper($this->db,'paypal/1.0/config.xml');
        $fields = ['order_id' => $order_id];
        $paypal = $em->find_by_fields($fields,1)[0];
        $sales_response = json_decode($paypal->field('payment_info'));

        $pp = new Paypal();
        $pp->init($this->app_config->paypal());
        $pp->generate_invoice_for_checkout($sales_response);
        $pp->send_invoice();
        $pp->mark_invoice_as_paid();
        $pp->invoice_details();

        $paypal->set_field('invoice_info', $pp->invoice_response());
        Entity_mapper::save_entity_to_storage($paypal, $this->db);
    }

}