<?php

class Execute_payment_action extends AF\Action
{
    public function execute()
    {
        $paymentID = $_POST['paymentID'];
        $payerID = $_POST['payerID'];

        $pp = new Paypal();
        $pp->init($this->app_config->paypal());
        $pp->execute_payment($paymentID, $payerID);

        //$sales_response = json_decode($pp->sale_response());
        //$inr = $sales_response->transactions[0]->invoice_number;

        $order_entity = new Entity('order/1.0/config.xml');
        $order_entity->set_field('product_id', 1);
        //$order_entity->set_field('invoice_number', $inr);
        Entity_mapper::insert_entity_to_storage($order_entity, $this->db);

        $pp_entity = new Entity('paypal/1.0/config.xml');
        $pp_entity->set_field('order_id', $order_entity->id());
        $pp_entity->set_field('payment_info', $pp->sale_response());
        Entity_mapper::insert_entity_to_storage($pp_entity, $this->db);
    }


}