<?php

class Rest
{
    private $url = '';
    private $auth_method = '';
    private $auth_user = '';
    private $auth_password = '';
    private $auth_token = '';
    private $post_format = 'json';
    private $return_format = 'json';

    public function __construct($url = '')
    {
        $this->set_url($url);
    }

    /**
     * sets url without trailing '/'
     * @param string    url
     */
    public function set_url($url)
    {
        $this->url = rtrim($url, '/');
    }

    /**
     * sets authentication method and data
     * @param string    jwt, webauth, bearer, none
     * @param mixed
     */
    public function set_auth_mode($mode = "none", $data = false)
    {
        switch ($mode) {
            case 'jwt':
                $this->auth_method = "jwt";
                $this->auth_token = $data;
                break;

            case 'bearer':
                $this->auth_method = "bearer";
                $this->auth_token = $data;
                break;

            case 'webauth':
                $this->auth_method = "webauth";
                $this->auth_user = $data['login'];
                $this->auth_password = $data['password'];
                break;

            case 'x_auth_token':
                $this->auth_method = "x_auth_token";
                $this->auth_token = $data;
                break;

            default:

                break;
        }
    }

    /**
     * set POST format
     * @param string
     */
    public function set_post_format($post_format)
    {
        $this->post_format = $post_format;
    }

    /**
     * POST request
     * @param string
     * @param string
     * @param string
     * @return string
     */
    public function post($path, $data = '', $mode = "post")
    {
        $curl_post_data = $data;

        $curl = curl_init($this->url . $path);
        $this->set_curl_header($curl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        switch ($mode) {
            case 'put':
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                break;

            default:
                curl_setopt($curl, CURLOPT_POST, true);
                break;
        }

        if (!empty($curl_post_data))
        {
            if ($this->post_format == "json")
            {
                if (is_string($curl_post_data)) {
                    if (!$curl_post_data = json_decode($curl_post_data)) {
                        //throw new \Exception("data is no JSON");
                    }
                }

                $curl_post_data = json_encode($curl_post_data);
            }

            curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
        }

        $curl_response = curl_exec($curl);

        $headers = curl_getinfo($curl);

        curl_close($curl);

        return $this->return_format == 'json' ? json_decode($curl_response) : $curl_response;
    }

    public function get($path, $data = '', $mode = "get")
    {
        $url = $this->url . $path;
        if (!empty($data))
        {
            $url .= '?' . http_build_query($data);
        }

        $curl = curl_init($url);
        $this->set_curl_header($curl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        //debug
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);

        // delete is from clever reach rest client
        /*switch ($mode) {
            case 'delete':
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, strtoupper($mode));
                break;

            default:
                break;
        }*/

        $curl_response = curl_exec($curl);
        $headers = curl_getinfo($curl);
        /*echo "headers <br/>";
        echo "<pre>";
        print_r($headers);
        echo "</pre>";*/

        curl_close($curl);

        return $this->return_format == 'json' ? json_decode($curl_response) : $curl_response;
    }


    public function delete($path, $data = '')
    {
        return $this->get($path, $data, "delete");
    }

    public function put($path, $data = '')
    {
        return $this->post($path, $data, "put");
    }

    /**
     * sets curl header and auth
     * @param  curl_pointer
     */
    private function set_curl_header(&$curl)
    {

        $header = array();

        switch ($this->post_format) {
            case 'json':
                $header['content'] = 'Content-Type: application/json';
                break;

            default:
                $header['content'] = 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8';
                break;
        }

        switch ($this->auth_method) {
            case 'webauth':
                curl_setopt($curl, CURLOPT_USERPWD, $this->auth_user . ":" . $this->auth_password);
                break;

            case 'jwt':
                $header['token'] = 'X-ACCESS-TOKEN: ' . $this->auth_token;
                break;

            case 'bearer':
                $header['token'] = 'Authorization: Bearer ' . $this->auth_token;
                break;

            case 'x_auth_token':
                $header['token'] = 'X-Auth-Token: api-key ' . $this->auth_token;
                break;

            default:
                break;
        }
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
    }
    
}