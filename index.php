<!DOCTYPE html>
<html lang="de"><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Zinsempfehlung</title>
    <link rel="icon" type="image/x-icon" href="img/icon.ico">

    <link rel="stylesheet" href="app-foundation/css/dist/af.css">
    <link rel="stylesheet" href="css/dist/style.css">

    <script src="vendor/dist/vendor.min.js"></script>
    <script src="app-foundation/js/dist/af.min.js"></script>


    <link rel='stylesheet' id='cookie-notice-front-css'  href='https://www.zinsempfehlung.de/wp-content/plugins/cookie-notice/css/front.min.css?ver=4.8.1' type='text/css' media='all' />

    <script type='text/javascript'>
        /* <![CDATA[ */
        var cnArgs = {"ajaxurl":"https:\/\/www.zinsempfehlung.de\/wp-admin\/admin-ajax.php","hideEffect":"fade","onScroll":"no","onScrollOffset":"100","cookieName":"cookie_notice_accepted","cookieValue":"TRUE","cookieTime":"2592000","cookiePath":"\/","cookieDomain":"","redirection":"","cache":""};
        /* ]]> */
    </script>
    <script type='text/javascript' src='https://www.zinsempfehlung.de/wp-content/plugins/cookie-notice/js/front.min.js?ver=1.2.42'></script>



    <?php
       $code = $_GET['code'];
    ?>

</head>
<body>
<?php
echo "<input type='hidden' name='short_id' id='short_id' value='$code' >";
?>
<main>
    <section class="bg-basiccolor margNeg">
        <div class="container-fluid">
            <div class="grid_12 center pad20">
                <h1 class="uppercase">MONEYWELL – DIE INTERESSANTE ALTERNATIVE ZU HERKÖMMLICHEN GELDANLAGEN</h1>
                <p class="big140 weight100">Wie Du mit MONEYWELL unschlagbare Umsätze als Tippgeber schreibst, die wirklich für Dich automstisiert Verkaufen - versprochen!</p>
            </div>
        </div>
    </section>

    <section>
        <div class="container-fluid">
            <div class="grid_12 pad150">
                <div class="grid_7">
                    <div class="mute" id="video">
                        <div class="js-mute">
                            <img src="img/icons/icon-ton-ffffff.svg">
                        </div>
                        <video autobuffer="" autoplay="" muted="" controls="">
                            <source src="http://wincent-online.de/_media/videos/kunde/Moneywell_AllesOnline_WA.mp4" type="video/mp4">
                        </video>
                    </div>
                </div>
                <div class="grid_5 pad20 pad0-t" style="margin-top: 70px;">
                        <div class="grid_12 marg150">
                            <label for="news" class="small80" style="display: block;text-align: center;margin: 0 0 12.5px; font-size: 110%;">Sichern Sie sich jetzt die besten Investitionschancen vor allen anderen.</label>
                        </div>
                        <button type="submit" class="btn_04 width100" onclick="window.location.href='https://www.moneywell.de/projekte?network_id=zinsempfehlung&utm_source=<?php echo $code; ?>'">Jetzt registrieren</button>
                      <!--  <a href="https://www.moneywell.de/projekte?network_id=zinsempfehlung&utm_source=2c89U" class="btn_04 width100">Jetzt registrieren</a> -->
                </div>
            </div>
        </div>
    </section>

    <section data-section="1">
        <hr>
        <div class="container-fluid">
            <div class="grid_10 preffix_1 marg300">
                <h2 class="center">MONEYWELL – DIE INTERESSANTE ALTERNATIVE ZU HERKÖMMLICHEN GELDANLAGEN</h2>
            </div>
            <div class="grid_10 preffix_1  marg0-t marg300 width-tablet-wide-100">
                <h3 class="center">So <span class="line">einfach</span> geht es!</h3>
                <div class="grid_12 kreise">
                    <div class="kreis-3">
                        <div class="bg-schritt-1">
                            <img src="img/konto_erstellen.png" />
                        </div>
                    </div>
                    <div class="kreis-3">
                        <div class="bg-schritt-2">
                            <img src="img/projekt.png" />
                        </div>
                    </div>
                    <div class="kreis-3">
                        <div class="bg-schritt-3">
                            <img src="img/kreislauf.png" />
                        </div>
                    </div>
                </div>
                <div class="grid_12">
                    <div class="grid_4 center">
                        <p class="big160">1.</p>
                        <p class="big120">Finden Sie Ihr Investmentprojekt</p>
                    </div>
                    <div class="grid_4 center">
                        <p class="big160">2.</p>
                        <p class="big120">Erstellen Sie Ihr persönliches Konto</p>
                    </div>
                    <div class="grid_4 center">
                        <p class="big160">3.</p>
                        <p class="big120">Online investieren und Zinsen sichern</p>
                    </div>
                </div>
            </div>


                <div class="grid_8 preffix_2 marg150 center">
                    <button type="button" class="btn_01" onclick="window.location.href='https://www.moneywell.de/projekte?network_id=zinsempfehlung&utm_source=<?php echo $code; ?>'">Jetzt gleich registrieren</button>
                </div>

        </div>
    </section>

    <section class="bg-basiccolor" data-section="2">
        <div class="container-fluid">
            <div class="grid_12 marg300">
                <h2 class="center">Keine gewöhnliche Geldanlage</h2>
                <div class="pad150 marg300">
                    <div class="round border big"><img src="img/icons/zusatzverdienst.svg" /> </div>
                    <h3 class="uppercase">Attraktive Konditionen</h3>
                    <p>Lukrative Zinsen zwischen 3 – 6 % statt durchschnittl. 0,5 % bei Ihrer Bank.<br>
                        Sie können bereits ab 100 € Anlagekapital investieren und haben kurze Laufzeiten.</p>
                </div>
                <div class="pad150">
                    <div class="round border big"><img src="img/icons/zukunft.svg" /> </div>
                    <h3 class="uppercase">Wertstabilität</h3>
                    <p>Sachwerte unterliegen im Regelfall, im Gegensatz zu spekulativen Anlageformen, keinen signifikanten Wertschwankungen. Sie bieten bei sorgfältiger Auswahl die Möglichkeit, Vermögenswerte zu erhalten und zu vermehren. Ihr Investment ist börsenunabhängig und ist nicht von schwankenden Aktienkursen betroffen.</p>
                </div>
                <div class="pad150 marg300">
                    <div class="round border big"><img src="img/icons/grown_up.svg" /> </div>
                    <h3 class="uppercase">Grown-up Investments</h3>
                    <p>Sie investieren in bereits bestehende, etablierte Unternehmen, nicht in unkalkulierbare Start-Up-Ideen.</p>
                </div>
                <div class="grid_8 preffix_2 marg150 center">
                    <button type="button" class="btn_01" onclick="window.location.href='https://www.moneywell.de/projekte?network_id=zinsempfehlung&utm_source=<?php echo $code; ?>'">Jetzt gleich registrieren</button>
                </div>
            </div>
        </div>
    </section>

    <!-- <section class="bg-grau">
         <div class="container-fluid bg-image">
             <div class="grid_12 marg300">
                 <div class="grid_6 preffix_6">
                     <p class="big140">Spare Zeit, Geld und Stress mit dem
                         digitalen Partnerprogramm mit Real-
                         Time Kundengewinnung, automatisierter
                         Verwaltung und integriertem
                         Abrechnungssystem der Anleger.</p>
                 </div>
             </div>
         </div>
     </section>

      <section>
         <div class="container-fluid">
             <div class="grid_12 marg300">
                 <ul class="img-list">
                     <li><img src="img/logos/Sachwert_Magazin_Moneywell.jpg"/> </li>
                     <li><img src="img/logos/finanzwelt_Moneywell.jpg"/> </li>
                     <li><img src="img/logos/MOIN-HH.jpg"/> </li>
                     <li><img src="img/logos/Creditreform_Moneywell.jpg"/> </li>
                     <li><img src="img/logos/CashOnline_Moneywell.jpg"/> </li>
                     <li><img src="img/logos/Wirtschafts_TV_Moneywell.jpg"/> </li>
                 </ul>
             </div>
         </div>
     </section>

     <section>
         <div class="container-fluid">
             <div class="grid_12 marg300 center">
                 <h2 class="marg0-t">Bereits zufriedene Kunden</h2>
                 <div class="grid_7 ">
                     <img src="img/Bewertungen.jpg" alt="Kundenbewertungen von ausgezeichnet.org" />
                 </div>
                 <div class="grid_5">
                     </div>
             </div>
         </div>
     </section>
 -->
    <?php
        if(empty($code) || strlen ($code) < 5){
            ?>
            <section  id="overlay">
                <div class="center">
                    <h2>Empfehlung erhalten?</h2>
                    <p class="big120">Gib hier den Empfehlungscode
                        Deines Freundes ein.</p>
                    <form id="form-code">
                        <input type="text" class="js-code" name="code1" id="code1" data-nr="1" placeholder="1">
                        <input type="text" class="js-code" name="code2" id="code2" data-nr="2" placeholder="2">
                        <input type="text" class="js-code" name="code3" id="code3" data-nr="3" placeholder="3">
                        <input type="text" class="js-code" name="code4" id="code4" data-nr="4" placeholder="4">
                        <input type="text" class="js-code" name="code5" id="code5" data-nr="5" placeholder="5">
                        <p>Sobald Du deinen 6 stelligen Empfehlungscode
                            eingetragen hast wird die Partnerseite frei geschalten.</p>
                        <button type="submit" class="btn_0">Code einlösen</button>
                    </form>
                </div>
            </section>
            <?php
        }
    ?>

    <script type="text/javascript">
        let elemente = document.getElementsByClassName('js-code');
        if(undefined !== elemente || null !== elemente){
            for(let i = 0; i < elemente.length; i++){
                elemente[i].addEventListener('keyup', function (event) {
                    let aktNr = parseInt(this.getAttribute('data-nr')),
                        neueNr = aktNr + 1,
                        neueId = 'code'+neueNr,
                        neuesElement = document.getElementById(neueId);
                    if(null !== neuesElement){
                        neuesElement.focus();
                    }
                });
            }
        }

        let form = document.getElementById('form-code');
        if(undefined !== form && null !== form){
            form.onsubmit = function(ev){
                ev.preventDefault();
                let code = '';
                for(let i = 0; i < (this.elements.length - 1); i++){
                    code = code + this.elements.item(i).value;
                }
                /*if(code === '') {
                    code = '2c89U';
                } */
                let url = document.location.pathname.split('/')
                document.location.href = document.location.origin+'/'+url[1]+'/'+code;
            };
        }

        checkShortId();
        function checkShortId(){
            let short_id = document.getElementById('short_id').value;
            if(short_id === '' || short_id < 5){
                showOverlay();
            } else {
                loadDataUserId();
            }
        }

        function loadDataUserId(){

            let short_id = document.getElementById('short_id').value;

            xhr=new XMLHttpRequest();

            xhr.onreadystatechange=function()
            {
                if (xhr.readyState==4 && xhr.status==200)
                {
                    console.log(xhr.responseText);
                   //debugger;
                    let response = xhr.responseText;
                    const obj = JSON.parse(response);
                    if(obj.data.length > 0){
                        loadDataUserLanding(obj.data[0].id);
                    } else {
                        showOverlay();
                    }
                }
            };

            xhr.open('POST', 'api', true);
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

            data = {
                event: {
                    name: 'entity.load',
                    data: {
                        entity_name: 'user',
                        entity_filter: {short_id: short_id},
                        entity_fields: ['id']
                    }
                }
            };

            xhr.send("event="+JSON.stringify(data.event));
            return xhr;
        }

        function loadDataUserLanding(id) {
            xhr=new XMLHttpRequest();

            xhr.onreadystatechange=function()
            {
                if (xhr.readyState==4 && xhr.status==200)
                {
                    let response = xhr.responseText;
                    console.log(response);
                    const obj = JSON.parse(response);
                    if(obj.data.length > 0){
                        setUserDataLanding(obj.data[0]);
                    }
                }
            };

            xhr.open('POST', 'api', true);
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

            data = {
                event: {
                    name: 'entity.load',
                    data: {
                        entity_name: 'personal_landingpage',
                        entity_filter: {user_id: id}
                    }
                }
            };

            xhr.send("event="+JSON.stringify(data.event));
            return xhr;
        }

        function showOverlay(){
            document.getElementById('overlay').style.display = 'block';
        }

        function setUserDataLanding(data){
            let src = 'http://wincent-online.de/_media/videos/kunde/Moneywell_AllesOnline_WA.mp4';
            switch (data.video_kunde){
                case '2':
                    src = 'http://wincent-online.de/_media/videos/kunde/Moneywell_GeldMussArbeiten_WA.mp4';
                    break;
                case '3':
                    src = 'http://wincent-online.de/_media/videos/kunde/Moneywell_LukrativeZinsen_WA.mp4';
                    break;
                case '4':
                    src = 'http://wincent-online.de/_media/videos/kunde/Moneywell_Wirtschaftsboom_WA.MP4';
                    break;
            }
            document.getElementById('video').getElementsByTagName('video')[0].src = src;

            let sections = document.getElementsByTagName('section');
            for(let i = 0; i < sections.length; i++){
                let section = sections[i],
                    attr = section.getAttribute('data-section');
                if(null !== attr){
                    setSectionFromUserData(attr, data, section);
                }
            }
        }

        function setSectionFromUserData(nr, data, element){
            nr = parseInt(nr);
            switch (nr){
                case 1:
                    (data.div1_kunde === '0')? element.style.display = 'none' : '';
                    break;
                case 2:
                    (data.div2_kunde === '0')? element.style.display = 'none' : '';
                    break;
            }
        }

        $('.js-mute').on('click', function (ev) {
            $(this).parent().find('video').prop('muted', false);
            $(this).parent().addClass('off');
            ev.preventDefault();
        });
    </script>

</main>
<footer>
    <p>Moneywell<sup>©</sup>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="https://www.moneywell-vertrieb.de/impressum/" target="_blank">Impressum</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="https://www.moneywell-vertrieb.de/datenschutz/" target="_blank">Datenschutz</a></p>
</footer>

<script type="text/javascript">
    setIframeHeight();

    window.onresize = function(event) {
        setIframeHeight();
    };

    function setIframeHeight() {
        let width = document.getElementsByTagName('iframe')[0].clientWidth,
            height = width * 0.56;
        document.getElementsByTagName('iframe')[0].height = height;
    }
</script>


<div id="cookie-notice" role="banner" class="cn-top wp-default" style="color: #fff; background-color: #a4a4a4;">
    <div class="cookie-notice-container">
        <span id="cn-notice-text">Wir verwenden Cookies, um Inhalte und Anzeigen zu personalisieren und die Zugriffe auf unsere Website zu analysieren. Außerdem geben wir Informationen zu Ihrer Nutzung unserer Website an unsere Partner für soziale Medien, Werbung, Analysen und an die Moneywell GmbH weiter.
            <a href="https://www.zinsempfehlung.de/datenschutz/" style="color: #ff6a00;">Details.</a>
        </span>
        <a href="#" id="cn-accept-cookie" data-cookie-set="accept" class="cn-set-cookie button wp-default" style="background: #ff6a00; color: white;">OK</a>
    </div>
</div>


</body>
</html>