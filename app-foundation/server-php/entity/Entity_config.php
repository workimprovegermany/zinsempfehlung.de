<?php

class Entity_config
{
    private $path;
    private $name;
    private $description;
    private $tablename;

    static private $model_path = null;
    static private function model_path()
    {
        if (self::$model_path == null)
        {
            $app_config = new Config(APP_CONFIG);
            $app_path = APP_ROOT_PATH.$app_config->entry('app_path');
            $model_path = $app_path.'/models';
            self::$model_path = $model_path;
        }

        return self::$model_path;
    }
    public static function create($config_path)
    {
        $file = self::model_path().'/'.$config_path;
        $config_xml = new SimpleXMLElement(file_get_contents($file));

        $impl = $config_xml->xpath('/config/implementation');

        if (!empty($impl))
        {
            $impl_class = $impl[0]->__toString();
            $impl_file = APP_ROOT_PATH.'/'.$impl_class.'.php';
            if (file_exists($impl_file))
            {
                require_once($impl_file);
                return new $impl_class($config_path);
            }
        }

        return new Entity($config_path);
    }



    public function __construct($path)
    {
        //$e = new \Exception;
        //echo "<pre>";
        //var_dump($e->getTraceAsString());
        //echo "</pre>";

        $this->path = $path;

        $config_xml = new SimpleXMLElement(file_get_contents($this->get_absolute_path()));
        //echo "config_xml: <pre>".print_r($config_xml,true). "</pre>";

        $name = $config_xml->xpath('/config/name')[0]->__toString();
        $this->name = $name;

        $description = $config_xml->xpath('/config/description')[0]->__toString();
        $this->description = $description;

        $tablename = $config_xml->xpath('/config/table_name')[0]->__toString();
        $this->tablename = $tablename;
    }

    public function get_fields()
    {
        $config_xml = new SimpleXMLElement(file_get_contents(self::model_path().'/'.$this->path));

        $field_names = $config_xml->xpath("/config/fields/field/name");

        $ret = array();
        foreach ($field_names as $field_name)
        {
            array_push($ret, $field_name->__toString());
        }

        return $ret;
    }

    public function field_description($name)
    {
        $config_xml = new SimpleXMLElement(file_get_contents(self::model_path().'/'.$this->path));

        $field_entries = $config_xml->xpath("/config/fields/field[name = '$name']/*");

        $ret = array();
        foreach ($field_entries as $field_entry)
        {
            $ret[$field_entry->getName()] = $field_entry->__toString();
        }

        return $ret;
    }

    public function get_absolute_path()
    {
        $file = self::model_path().'/'.$this->path;
        return $file;
    }

    public function get_table_name()
    {
        return $this->tablename;
    }

    public function get_model()
    {
        return $this->path;
    }

    public function get_relation_keys()
    {
        $config_xml = new SimpleXMLElement(file_get_contents("../models/".$this->path));

        $rels = $config_xml->xpath('/config/relations/relation');

        $ret = array();
        foreach ($rels as $rel)
        {
            array_push($ret, $rel->name[0]->__toString());
        }

        return $ret;
    }

    public function get_relation_type($relation)
    {
        $config_xml = new SimpleXMLElement(file_get_contents("../models/".$this->path));
        $rels = $config_xml->xpath("/config/relations/relation[name=\"$relation\"]");
        $ret = $rels[0]->type[0]->__toString();
        return $ret;
    }

}