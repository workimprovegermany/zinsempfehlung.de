<?php
/**
 * Created by PhpStorm.
 * User: marco
 * Date: 28.11.17
 * Time: 15:06
 */

class Active_campaign
{
    private $base_url;
    private $api_key;
    private $timeout;

    private $list_id;

    public function __construct($config)
    {
        //TODO: test if $config contains valid values
        $this->base_url = $config['base_url'];
        $this->api_key = $config['api_key'];
        $this->timeout = $config['timeout'] / 1000;

        //$this->list_id = $this->get_first_list_id();
        $this->list_id = $config['list_id'];
    }

    public function set_list_id($id)
    {
        $this->list_id = $id;
    }

    public function create_update_user($teilnehmer, $export_config)
    {
        $export_fields = array(
            "vorname" => "Vorname",
            "name" => "Name",
            "email" => "E-Mail",
            "anrede" => "Anrede",
            "ansprache" => "Ansprache",
            "videotitel" => "Videotitel",
            "videountertitel" => "Videountertitel",
            "videoid" => "Webinar ID",
            "videodatum" => "Datum Teilnahme",
            "teilgenommen" => "teilgenommen",
            "strasse" => "Straße",
            "plz" => "PLZ",
            "ort" => "Ort",
        );

        // sollen in db stehen
        $imparare_field_names = array(
            "vorname" => "vorname",
            "name" => "name",
            "email" => "email",
            "anrede" => "anrede",
            "ansprache" => "ansprache",
            "videotitel" => "video_title",
            "videountertitel" => "video_untertitel",
            "videoid" => "video_id",
            "videodatum" => "teilgenommen_datum",
            "teilgenommen" => "teilgenommen",
            "strasse" => "strasse",
            "plz" => "plz",
            "ort" => "ort",
        );

        $required_export_fields = array('vorname', 'name', 'email');

        $post = array();
        $field_ids = $this->get_custom_fields_ids();
        foreach ($export_fields as $field => $descr)
        {
            // wenn Pflichtfeld oder Exporthaken gesetzt ist
            if (in_array($field, $required_export_fields) || $export_config[api_table_export_column_name($field)] == 1)
            {
                // übernehmen bedeutet im array zu setzen: mailer_feldname => wert_von(imparare_feldname)
                if (in_array($field, $required_export_fields))
                {
                    $post_field_name = $export_config[api_table_mailer_column_name($field)];
                }
                else
                {
                    $post_field_name = 'field['.array_search($export_config[api_table_mailer_column_name($field)], $field_ids).',0]';
                }

                $post[$post_field_name] = $teilnehmer->nice_field($imparare_field_names[$field]);
            }
        }

        echo "export: ";
        print_r($post);
        echo "<br/><br/><br/>";

        $params = array(
            'api_key'      => $this->api_key,
            'api_action'   => 'contact_add',
            'api_output'   => 'json', // valid values: xml,json,serialize
        );

        // add list and status
        $list_field = "p[$this->list_id]";
        $status_field = "status[$this->list_id]";
        $post[$list_field] = $this->list_id;
        $post[$status_field] = 1;

        $query = "";
        foreach( $params as $key => $value ) $query .= urlencode($key) . '=' . urlencode($value) . '&';
        $query = rtrim($query, '& ');

        $data = "";
        foreach( $post as $key => $value ) $data .= urlencode($key) . '=' . urlencode($value) . '&';
        $data = rtrim($data, '& ');

        $url = rtrim($this->base_url, '/ ');

        $api = $url . '/admin/api.php?' . $query;


        $ch = curl_init($api);

        curl_setopt($ch, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data); // use HTTP POST to send form data
        //curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment if you get no gateway response and are using HTTPS
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch);
        curl_close($ch);

        $res_dec = json_decode($result);

        echo "<pre>";
        print_r($res_dec);
        echo "</pre>";

        echo "<pre>";
        print_r($httpCode);
        echo "</pre>";

        return true;
    }

    public function test()
    {

        $ch = curl_init($this->base_url."lists/".$this->list_id."/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $auth = 'user:'.$this->api_key;
        curl_setopt($ch, CURLOPT_USERPWD, $auth);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        //for debug
        //curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);

        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch);
        curl_close($ch);

        $res_dec = json_decode($result);

        echo "<pre>";
        print_r($res_dec);
        echo "</pre>";

        echo "<pre>";
        print_r($httpCode);
        echo "</pre>";
    }

    function get_first_list_id()
    {
        $ch = curl_init('https://us16.api.mailchimp.com/3.0/lists');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $auth = 'user:'.$this->api_key;
        curl_setopt($ch, CURLOPT_USERPWD, $auth);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        //for debug
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);

        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch);
        curl_close($ch);

        $res_dec = json_decode($result);
        return $res_dec->lists[0]->id;
    }

    function get_custom_fields_ids()
    {
        $params = array(
            'api_key'      => $this->api_key,
            'api_action'   => 'list_field_view',
            'api_output'   => 'json',
            'ids'           => 'all',
        );

        $query = "";
        foreach( $params as $key => $value ) $query .= urlencode($key) . '=' . urlencode($value) . '&';
        $query = rtrim($query, '& ');

        $url = rtrim($this->base_url, '/ ');
        $api = $url . '/admin/api.php?' . $query;

        $request = curl_init($api); // initiate curl object
        curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
        //curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment if you get no gateway response and are using HTTPS
        curl_setopt($request, CURLOPT_FOLLOWLOCATION, true);

        $response = curl_exec($request); // execute curl fetch and store results in $response

        curl_close($request); // close curl object

        $result = json_decode($response, true);

        echo 'The entire result printed out:<br />';
        echo '<pre>';
        print_r($result);
        echo '</pre>';

        // loop over array and generate new array field_id => 'field_name'
        $fields = array();
        foreach ($result as $item)
        {
            if (is_array($item) && isset($item['id']) && isset($item['title']))
            {
                $fields[$item['id']] = $item['title'];
            }
        }

        echo 'fields array:<br />';
        echo '<pre>';
        print_r($fields);
        echo '</pre>';

        return $fields;
    }
}

function api_table_export_column_name($fieldname)
{
    return "p_".$fieldname."_export";
}
function api_table_imparare_column_name($fieldname)
{
    return "p_".$fieldname."_imparare";
}
function api_table_mailer_column_name($fieldname)
{
    return "p_".$fieldname."_mailer";
}

