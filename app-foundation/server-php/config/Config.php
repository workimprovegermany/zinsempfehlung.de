<?php

class Config
{
    private $config_xml;

    public function __construct($config_file)
    {
        $this->config_xml = new Xml_document($config_file);
    }

    public function entry($key)
    {
        return $this->config_xml->query_string("/config/*[name() = '$key']");
    }

    public function server_id()
    {
        $server_name = $_SERVER['SERVER_NAME'];
        //$server_id = $this->config_xml->query_string("/config/server[url = \"$server_name\"]/@id");
        // TODO: regex support

        //with XPath 2.0 this would be: ends-with('$server_name', url)
        $server_id = $this->config_xml->query_string("/config/server[url = substring('$server_name', string-length('$server_name') - string-length(url) + 1 )]/@id");

        return $server_id;
    }

    public function db()
    {
        $server_id = $this->server_id();
        $db_settings = array();

        $db_settings['host'] = $this->config_xml->query_string("/config/db[@for_server = \"$server_id\"]/host");
        $db_settings['db_name'] = $this->config_xml->query_string("/config/db[@for_server = \"$server_id\"]/db_name");
        $db_settings['db_user'] = $this->config_xml->query_string("/config/db[@for_server = \"$server_id\"]/db_user");
        $db_settings['db_pass'] = $this->config_xml->query_string("/config/db[@for_server = \"$server_id\"]/db_pass");

        return $db_settings;
    }

    public function paypal()
    {
        $server_id = $this->server_id();
        $paypal_settings = array();

        $paypal_settings['api_url'] = $this->domain_subdir().'/api';
        $paypal_settings['return_url'] = $this->domain_subdir().$this->config_xml->query_string("/config/paypal[@for_server = \"$server_id\"]/return_url");
        $paypal_settings['cancel_url'] = $this->domain_subdir().$this->config_xml->query_string("/config/paypal[@for_server = \"$server_id\"]/cancel_url");
        $paypal_settings['error_url'] = $this->domain_subdir().$this->config_xml->query_string("/config/paypal[@for_server = \"$server_id\"]/error_url");


        $paypal_settings['login'] = $this->config_xml->query_string("/config/paypal[@for_server = \"$server_id\"]/login");
        $paypal_settings['password'] = $this->config_xml->query_string("/config/paypal[@for_server = \"$server_id\"]/password");

        $merchant_info = array();
        $merchant_info['email'] = $this->config_xml->query_string("/config/paypal[@for_server = \"$server_id\"]/merchant_info/email");
        $merchant_info['business_name'] = $this->config_xml->query_string("/config/paypal[@for_server = \"$server_id\"]/merchant_info/business_name");
        $merchant_info['phone'] = array(
            'country_code' => $this->config_xml->query_string("/config/paypal[@for_server = \"$server_id\"]/merchant_info/phone/country_code"),
            'national_number' => $this->config_xml->query_string("/config/paypal[@for_server = \"$server_id\"]/merchant_info/phone/national_number"),
            );
        $paypal_settings['merchant_info'] = $merchant_info;

        return $paypal_settings;
    }

    public function entities()
    {
        $e = $this->config_xml->query("/config/entities/entity/name");

        $ret = array();

        foreach ($e as $item)
        {
            array_push($ret, $item->nodeValue);
        }

        return $ret;
    }

    public function mail_settings($name)
    {
        $e = $this->config_xml->query("/config/mails/".$name."/*");

        $ret = array();

        foreach ($e as $item)
        {
            $ret[$item->nodeName] = $item->nodeValue;
        }

        return $ret;
    }

    public function doc_settings($name)
    {
        $e = $this->config_xml->query("/config/doc_pfad/".$name."/*");

        $ret = array();

        foreach ($e as $item)
        {
            $ret[$item->nodeName] = $item->nodeValue;
        }

        return $ret;
    }

    public function entity_config_for_name($entity_name)
    {
        $c = $this->config_xml->query("/config/entities/entity[name = '$entity_name']/config")[0]->nodeValue;

        return $c;

    }

    public function entity_array()
    {
        $e = $this->config_xml->query("/config/entities/entity");

        $ret = array();

        foreach ($e as $item)
        {
            $entity_array = array();

            foreach ($item->getElementsByTagName('*') as $cn)
            {
                $entity_array[$cn->nodeName] = $cn->nodeValue;
            }

            array_push($ret, $entity_array);
        }

        return $ret;
    }

    public function entity_description($name)
    {
        $e = $this->config_xml->query("/config/entities/entity[name = '$name']/*");

        $ret = array();

        foreach ($e as $item)
        {
            $ret[$item->nodeName] = $item->nodeValue;
        }

        return $ret;
    }

    public function events()
    {
        $e = $this->config_xml->query("/config/events/event");

        $ret = array();

        foreach ($e as $item)
        {
            $ret[$item->getAttribute('id')] = $item->getElementsByTagName('action')[0]->nodeValue;
        }

        return $ret;
    }

    public function domain_subdir()
    {
        $server_id = $this->server_id();
        $server_subdir = $this->config_xml->query_string("/config/server[@id = '$server_id']/subdir");

        return '/'.$server_subdir;
    }

    public function domain_dir()
    {
        $link = $_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI'];
        $x = pathinfo($link);

        $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' ? 'https' : 'http') . '://' . $x['dirname'];
        return $url;
    }

}