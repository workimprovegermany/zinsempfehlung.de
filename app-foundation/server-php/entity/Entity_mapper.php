<?php


class Entity_mapper
{
    /**
     * @var Storage_adapter
     */
    private $adapter;

    /**
     * @var Entity_config
     */
    private $config = null;
    private $sort_key = null;
    private $sort_order = null;

    /**
     * @param Storage_adapter $storage
     * @param string $config
     */
    public function __construct(Storage_adapter $storage, $config = '')
    {
        $this->adapter = $storage;

        if (!empty($config))
        {
            $cfg = new Entity_config($config);
            $this->set_config($cfg);
        }
    }

    /**
     * @param Entity_config $config
     */
    public function set_config(Entity_config $config)
    {
        $this->config = $config;
    }


    public function set_sort($key = null, $order = null)
    {
        $this->sort_key = $key;
        $this->sort_order = $order == 'DESC' ? 'DESC' : null;
    }

    /**
     * find entity with $id from storage
     * returns Entity object
     *
     * @param int $id
     *
     * @return Entity
     */
    public function find_by_id($id)
    {
        $result = $this->adapter->find($id, $this->config->get_table_name());

        if (is_array($result))
        {
            return $this->map_row_to_entity($result);
        }

        return null;
    }

    /**
     * find entities from storage filtered by fields (key => value)
     * returns Entity object
     *
     * @param array $fields contains filters as (key => value) pairs
     * @param int $limit (optional)
     * @param int $offset (optional)
     *
     * @return Entity[]|null
     */
    public function find_by_fields($fields, $limit = null, $offset = null)
    {
        $result = $this->adapter->find_by_fields($fields, $this->config->get_table_name(), $limit, $offset, $this->sort_key, $this->sort_order);

        $ret = array();
        if (is_array($result))
        {
            foreach ($result as $e)
            {
                array_push($ret, $this->map_row_to_entity($e));
            }
            return $ret;
        }

        return null;
    }

    /**
     * find entity-ids from storage filtered by fields (key => value)
     * returns Entity object
     *
     * @param array $fields contains filters as (key => value) pairs
     * @param int $limit (optional)
     * @param int $offset (optional)
     *
     * @return int[]
     */
    public function find_ids_by_fields($fields, $limit = null, $offset = null)
    {
        $result = $this->adapter->find_ids_by_fields($fields, $this->config->get_table_name(), $limit, $offset);

        return $result;
    }

    /**
     * count entities in storage filtered by fields (key => value)
     * returns Entity object
     *
     * @param $fields[]
     *
     * @return int
     */
    public function count_by_fields($fields)
    {
        $result = $this->adapter->count_by_fields($fields, $this->config->get_table_name());

        return $result;
    }

    /**
     * count entities in storage filtered by fields (key => value)
     * returns Entity object
     *
     * @param $column
     * @param $fields[]
     *
     * @return int
     */
    public function sum_column_by_fields($column, $fields)
    {
        $result = $this->adapter->sum_column_by_fields($column, $fields, $this->config->get_table_name());

        return $result;
    }









    private function map_row_to_entity(array $row)
    {
        return Entity::from_state($row, $this->config->get_model());
    }



    public function update($id, $fields)
    {
        $this->adapter->update($this->config->get_table_name(), $id, $fields);
    }


    /**
     * save Entity
     *
     * @param Entity $entity
     *
     * @return
     */
    public function save($entity)
    {
        //TODO update last modification
        $entity_array = Entity::to_state($entity);
        unset($entity_array['id']);

        $this->adapter->update($this->config->get_table_name(), $entity->id(), $entity_array);
    }
    /**
     * save_entity_to_storage Entity
     *
     * @param Entity $entity
     * @param Storage_adapter $storage
     *
     * @return
     */
    public static function save_entity_to_storage(Entity $entity, Storage_adapter $storage)
    {
        $ec = new Entity_config($entity->config_path());

        $entity_array = Entity::to_state($entity);
        unset($entity_array['id']);

        $storage->update($ec->get_table_name(), $entity->id(), $entity_array);
    }


    public function insert(Entity $entity)
    {
        $entity_array = Entity::to_state($entity);
        unset($entity_array['id']);

        $ret = $this->adapter->insert($entity_array, $this->config->get_table_name());

        $entity->set_id($ret['last_id']);
        $entity->turn_into('PERSISTENT');

        return $ret;
    }

    public static function insert_entity_to_storage(Entity $entity, Storage_adapter $storage)
    {
        $ec = new Entity_config($entity->config_path());

        $entity_array = Entity::to_state($entity);
        unset($entity_array['id']);

        $ret = $storage->insert($entity_array, $ec->get_table_name());

        $entity->set_id($ret['last_id']);
        $entity->turn_into('PERSISTENT');

        return $ret;
    }



    public function delete($id)
    {
        $this->adapter->delete($id, $this->config->get_table_name());
    }

}
