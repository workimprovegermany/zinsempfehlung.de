<?php


class Create_payment_action extends AF\Action
{

    public function execute()
    {
        $prod_id = $this->data['product_id'];
        $em = new Entity_mapper($this->db,'product/1.0/config.xml');
        $prod = $em->find_by_id($prod_id);

        $op = $prod->field('offer_price');
        $op2 = substr($op, 0,strlen($op)-2).'.'.substr($op, -2);

        $ot = $prod->field('offer_tax');
        $ot2 = substr($ot, 0,strlen($ot)-2).'.'.substr($ot, -2);

        $item = array(
            'quantity' => '1',
            'name' => $prod->field('title'),
            'price' => $op2,
            'currency' => $prod->field('currency'),
            'description' => $prod->field('title'),
            'tax' => $ot2
        );

        $item_list = array();
        array_push($item_list, $item);

        $pp = new Paypal();
        $pp->init($this->app_config->paypal());
        $pp->create_payment_from_item_list($item_list);
        header('Content-type: application/json');
        echo $pp->payment_id_json();

    }
}