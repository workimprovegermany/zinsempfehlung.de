<?php


class Router
{
    public $path = '';
    private $host = '';
    private $routes = array();

    public function init()
    {
        $this->path = '';

        $url_comp_arr = parse_url(rawurldecode($_SERVER['REQUEST_URI']));
        if(isset($url_comp_arr['path']))
        {
            $this->path = trim($url_comp_arr['path'],'/');
        }
    }

    public function addRoute($name, Route $route)
    {
        array_push($this->routes, array(
            'name' => $name,
            'route'=> $route
        ));
    }

    public function set_host($host)
    {
        $this->host = $host;
    }

    public function run_old()
    {
        $route_found = false;

        //contains [0] -> controller; [1] -> action; [2...] -> parameters as key/value
        $path_arr = explode('/', $this->path);

        // TODO: app subdir should be handled not here
        //$path_app = array_shift($path_arr);
        //for incorrectly configured servers
        //if ($path_app == $this->host)
        //{
            // shift once more, because the host is the first entry
            //$path_app = array_shift($path_arr);
        //}

        $path_controller = array_shift($path_arr);
        $path_action = array_shift($path_arr);
        $path_parameters = $path_arr;

        /*echo "ROUTER " . "<br/>";
        echo "path " . $this->path . "<br/>";
        echo "path_controller " . $path_controller . "<br/>";
        echo "path_action " . $path_action . "<br/>";
        echo "path_parameters " . $path_parameters . "<br/>";*/

        foreach($this->routes as $route_obj)
        {
            $route_name = $route_obj['name'];
            $route = $route_obj['route'];

            $route_url = $route->url;
            //find string start and end
            $route_url_search_str = '^'.$route_url.'$';

            //echo "ROUTEMATCH: path: ".$this->path." route: (".$route_url_search_str.",".$route->controller.",".$route->action.")"."<br/>";

            //echo "route_url_search_str: $route_url_search_str <br/>";
            //echo "path_controller: $path_controller <br/>";

            //match controller
            if (preg_match('#'.$route_url_search_str.'#',$path_controller,$matches) && !$route_found)
            {
                //shift first element because ist contains the whole string
                array_shift($matches);

                $controllername = ucfirst ( $route->controller ).'_controller';

                if (!$route->is_subsite)
                {
                    $action = $path_action;
                    // if no action then defaultaction
                    if ($action == '') {$action = $route->action;};

                    $controller = new $controllername($path_controller);
                    //append matches for parameters
                    $ret = call_user_func_array(array($controller, 'execute'), array($action));
                    return $ret;
                }
                else
                {
                    //contains [0] -> controller; [1] -> action; [2...] -> parameters as key/value
                    $sub_path_arr = explode('/', $this->path);
                    $sub_path_controller = array_shift($sub_path_arr);
                    $sub_path_controller = array_shift($sub_path_arr);
                    $sub_path_action = array_shift($sub_path_arr);
                    $sub_path_parameters = $sub_path_arr;

                    $action = $sub_path_action;
                    // if no action then defaultaction
                    if ($action == '') {$action = $route->action;};

                    $controller = new $controllername($sub_path_controller);
                    //append matches for parameters
                    $ret = call_user_func_array(array($controller, 'execute'), array($action));
                    return $ret;
                }

                $route_found = true;
            }
            //else echo "2 <br/>";
        }
        if ($route_found == false)
        {
            //echo "ROUTE NOT MATCHED: path: ".$this->path." array: ";
            //print_r($path_arr);

            if (!empty($this->path))
            {
                return "../views/".$this->path.".php";
            }
            else return "../views/home.php";
        }
    }

    public function run()
    {
        // function should return matching route based on defined regex on $this->path
        $route_found = false;

        /*echo "ROUTER " . "<br/>";
        echo "path " . $this->path . "<br/>";
        */

        foreach($this->routes as $route_obj)
        {
            $route_name = $route_obj['name'];
            $route = $route_obj['route'];

            $route_url = $route->url();
            $route_url_search_str = '^'.$route_url.'$';

            //echo "ROUTEMATCH: path: ".$this->path." route: ".$route_url_search_str."<br/>";

            //echo "route_url_search_str: $route_url_search_str <br/>";

            //match controller
            if (preg_match('#'.$route_url_search_str.'#',$this->path,$matches))
            {
                if (!isset($matches[1])) $matches[1] = $matches[0]; // if only one match without parentheses

                $route->set_matches($matches);

                return $route;
            }

            //else echo "2 <br/>";
        }

        //echo "ROUTE NOT MATCHED: path: ".$this->path." array: ";
        //print_r($path_arr);

        return null;
    }

}