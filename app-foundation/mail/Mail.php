<?php

/**
 * Created by PhpStorm.
 * User: m.tannert
 * Date: 27.06.2017
 * Time: 15:30
 */
class Mail
{
    private $host = "";
    private $user = "";
    private $pass = "";
    private $secure = "tls";
    private $port = 587;

    private $from = "";
    private $from_name = "";

    private $address = array();
    private $bcc = array();

    private $subject = "";
    private $body = "";
    private $alt_body = "";
    private $attachment = array();

    private $error_info = "";

    public function __construct()
    {
    }

    public function set_host($host)
    {
        $this->host = $host;
    }

    public function set_auth($user, $pass)
    {
        $this->user = $user;
        $this->pass = $pass;
    }

    public function set_secure($secure, $port)
    {
        $this->secure = $secure;
        $this->port = $port;
    }

    public function set_from($from)
    {
        $this->from = $from;
    }

    public function set_from_name($from_name)
    {
        $this->from_name = $from_name;
    }

    public function add_address($address)
    {
        array_push($this->address, $address);
    }

    public function add_bcc($bcc)
    {
        array_push($this->bcc, $bcc);
    }

    public function set_subject($subject)
    {
        $this->subject = $subject;
    }

    public function set_body($body)
    {
        $this->body = $body;
    }

    public function set_alt_body($alt_body)
    {
        $this->alt_body = $alt_body;
    }

    public function add_attachment($attachment)
    {
        array_push($this->attachment, $attachment);
    }

    public function get_error_info()
    {
        return $this->error_info;
    }




    public function verify()
    {
        //TODO: check other properties
        if (empty($this->host)) {return false;};

        return true;
    }

    public function send()
    {
        include_once(FOUNDATION_PATH."../../vendor/phpmailer/PHPMailerAutoload.php");
        $mail = new PHPMailer();
        $mail->SetLanguage("de", "../../vendor/phpmailer/language/");
        //$mail->SMTPDebug = 3;
        $mail->SMTPSecure = $this->secure;
        $mail->Port = $this->port;
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        $mail->isSMTP();                              	// send via SMTP
        $mail->Host     = $this->host;       			       // SMTP servers
        $mail->SMTPAuth = true;                    // turn on SMTP authentication
        $mail->Username = $this->user;   // SMTP username
        $mail->Password = $this->pass;  // SMTP password
        $mail->From     = $this->from;
        $mail->FromName = $this->from_name;
        foreach ($this->address as $adr)
        {
            $mail->AddAddress($adr);
        }
        foreach ($this->bcc as $bcc)
        {
            $mail->AddBCC($bcc);
        }
        $mail->WordWrap = 50;  // set word wrap
        $mail->isHTML(true);  // send as HTML
        $mail->Subject  =  $this->subject;
        $mail->Body     =  $this->body;
        $mail->AltBody  =  $this->alt_body;

        foreach ($this->attachment as $a)
        {
            $mail->AddAttachment($a);
        }

        if(!$mail->Send())
        {
            $this->error_info = $mail->ErrorInfo;
            return false;
        }
        return true;
    }

}